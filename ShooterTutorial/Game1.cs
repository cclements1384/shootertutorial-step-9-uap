﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ShooterTutorial
{
    public class Game1 : Game
    {
        private const int DEFAULT_SCREEN_WIDTH = 800;
        private const int DEFAULT_SCREEN_HEIGTH = 480;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferWidth = DEFAULT_SCREEN_WIDTH;
            graphics.PreferredBackBufferHeight = DEFAULT_SCREEN_HEIGTH;
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            /* Init our screen manager and add a screens to it. */
            SCREEN_MANAGER.add_screen(new GameMenu(GraphicsDevice, Content));
            SCREEN_MANAGER.add_screen(new GameScreen(GraphicsDevice, Content));
            SCREEN_MANAGER.add_screen(new GameOver(GraphicsDevice, Content));

            /* Set the active screen to the game menu */
            SCREEN_MANAGER.goto_screen("gameMenu");

            /* Init the current screen */
            SCREEN_MANAGER.Init();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            /* Have the active screen initilize itself. */
            SCREEN_MANAGER.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            /* have the actrive screen update */
            SCREEN_MANAGER.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            /* Draw the active screen */
            SCREEN_MANAGER.Draw(gameTime);

            base.Draw(gameTime);
        }
    }
}